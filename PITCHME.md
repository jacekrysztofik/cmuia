#VSLIDE

#### Uwierzytelnienie i autoryzacja

#HSLIDE

### Typy zasobów

- plik (gniazdo, urządzenie itd)
- akcja kontrolera
- rekord bazy danych

#HSLIDE

### Poziomy dostępu

- gość
- użytkownik
- uprzywilejowany

#HSLIDE

### Dostęp do zasobu

@ol

- próba dostępu do zasobu (URI)
- sprawdzenie przywilejów
- najniższy przywilej (GOŚĆ)
- czy zasób pozwala na podniesienie?
- czy można ujawnić jego istnienie?

@olend

#HSLIDE

### Identyfikatory zasobów

- szeregowe
- UUID

#VSLIDE

### Uwierzytelnienie

- Symfony firewall
- Formularz, OAuth, LDAP, X509

```yml
# config/packages/security.yaml
security:
    # ...

    firewalls:
        main:
            # ...
            x509:
                provider: your_user_provider
```

#VSLIDE

### Mechanizmy

- FOSUserBundle
- Access Control
- Security Annotations
- Voter (Authenticated, Role, Hierarchy)
- Access Listener
- Authorization Checker (szablony)

#HSLIDE

### FOSUserBundle

- gotowiec (role, hierarchia, zdarzenia)
- dobrze udokumentowany
- łatwo go rozwijać

#HSLIDE

### Access Control

- Request Matcher (atrybuty żądania)
- Access Decision Manager (konsensus, jednogłośność)
- Trust Resolver (np. remembered/fully)
- konfiguracja w yml i php (a więc i w db)

#HSLIDE

### Request Matcher

```yml
    firewalls:
        api_login:
            pattern:    ^/api/login
            # ...
            user_checker: AppBundle\Security\UserChecker


        api:
            request_matcher: security.api_request_matcher
            # ...
            user_checker: AppBundle\Security\UserChecker
```

#HSLIDE

### Request Matcher

```php

class ApiRequestMatcher implements RequestMatcherInterface
{
    public function matches(Request $request){
        return !$request->isXmlHttpRequest() &&
            'application/json' == $request->attributes->get('media_type');
    }
}

```

#HSLIDE

### Security Annotations

- @has_role (bieżący użytkownik)
- @is_granted (sprawdzenie przywileju)

#HSLIDE

### Hierarchia ról

- Dana rola może zawierać inne role
- Przywileje można więc dziedziczyć
- definicja hierarchii w yml lub w bazie

#VSLIDE

### Authorization Checker

Sprawdzanie dla fragmentu akcji (@Security sprawdza przed metodą)

```php
        $checker = $this->get('security.authorization_checker');

        if ($checker->isGranted(User::ROLE_CONTACTCENTER)) {
            // set some attribute for form etc.
            // load some additional data
```


#VSLIDE

### Voter - kod (delegacja)

```php
    /**
     * completeMinimalProfile
     *
     * @Route("/complete-minimal-profile", name="complete_minimal_profile")
     * @param Request $request
     */
    public function completeMinimalProfile(Request $request)
    {
        $user = $this->getUser();
        $this->denyAccessUnlessGranted('EDIT', $user);

        $form = $this->createForm(PhoneType::class, $user, ['action' => $this->generateUrl('complete_minimal_profile')]);
```

#HSLIDE

### Voter - konfiguracja

```yml
services:
    PublicBundle\Security\Voter\:
        resource: '../../src/PublicBundle/Security/Voter'
        tags:
            - { name: security.voter }
```

#HSLIDE

### Voter - kod (dependency injection)

```php
    private $decisionManager;

    public function __construct(AccessDecisionManagerInterface $decisionManager)
    {
        $this->decisionManager = $decisionManager;
    }
```

#HSLIDE

### Voter - kod (wybór voterów)

```php
    protected function supports($attribute, $subject)
    {
        if (!array_key_exists($attribute, self::$_methods)) {
            return false;
        }

        if (!$subject instanceof Complaint) {
            return false;
        }

        return true;
    }
```

#HSLIDE

### Voter - kod (głosowanie - ogólnie)

```php
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        if (!$this->decisionManager->decide($token, ['ROLE_VIEW_COMPLAINT'])) {
            return VoterInterface::ACCESS_DENIED;
        }

        $user = $token->getUser();

        if (!$user instanceof User) {
            // the user must be logged in; if not, deny access
            return VoterInterface::ACCESS_DENIED;
        }

        return call_user_func_array([$this, self::$_methods[$attribute]], [$subject, $user, $token]);

        throw new \LogicException('This code should not be reached!');
    }
```

#HSLIDE

### Voter - kod (głosowanie - szczegółowo)

```php
    private function canAmend(Complaint $subject, User $user, TokenInterface $token)
    {
        if ($subject->isParty($user)) {
            return VoterInterface::ACCESS_GRANTED;
        }

        if ($this->decisionManager->decide($token, ['ROLE_REDIRECT_COMPLAINT'])
            || $this->decisionManager->decide($token, ['ROLE_MANAGE_COMPLAINT'])) {
            return VoterInterface::ACCESS_GRANTED;
        }

        return VoterInterface::ACCESS_DENIED;
    }
```

#VSLIDE

## Tematy do szczegółowego zbadania

- LDAP (AD) - integracja
- uwierzytelnianie certyfikaty

